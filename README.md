-- Python Program to produce radarchart from a matrix competency --

**Purpose**
To have a radar chart with level of a dev of skills related to this kind of skills.
Input data : csv file from notion
Output data : jpeg of radar chart for each family of skills, and each dev into a zipfile

**Matrice of competency**
Name
Family
Level (1 to 5)

**install dependencies**
pip install pandas (Dataframe)
pip install flask
pip install -U kaleido (Static image generation)
